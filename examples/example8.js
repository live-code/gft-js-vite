import axios from 'axios';

/*axios.get('https://jsonplaceholder.typicode.com/xxxxusers')
  .then(function (res) {
    console.log('done', res.data)
  })
  .catch(err => alert('ahia'))*/

fetch('https://jsonplaceholder.typicode.com/users/123')
  .then(res => {
    console.log(res)
    if (!res.ok) {
      throw new Error('Network error');
    }
    return res.json()

  })
  .then(res => console.log('result', res))
  .catch(e => console.log('err', e))

function login() {
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      setTimeout(() => {
        reject('ahia!')
      }, 2000)
    }, 2000)
  })
}



login()
  .then(function (res) {
    console.log('done', res)
  })
  .catch(err => {
    console.log('reject', err)
  })
  .finally(() => console.log('sempre'))

