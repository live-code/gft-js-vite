// ARRAY METHODS

const users = [
  {id: 11, name: 'Silvia', age: 16, gender: 'F', city: 'Gorizia'},
  {id: 22, name: 'Fabio', age: 17, gender: 'M', city: 'Trieste', zoom: 10},
  {id: 33, name: 'Lorenzo', age: 26, gender: 'M', city: 'Pordenone'},
  {id: 44, name: 'Lisa', age: 23, gender: 'F', city: 'Gorizia'}
];

// MUTABLE
// POST ... { name: 'Fabio'}
users.push({ id: 55, name: 'Fabio' })

// DELETE
const idToRemove = 22;
const index = users.findIndex(u => u.id === idToRemove)
users.splice(index, 1);

// PUT / PATCH
const formData = { id: 33, name: 'Lorenza', gender: 'F'}
const index2 = users.findIndex(u => u.id === formData.id)
users[index2] = {...users[index2], ...formData}

