// destructuring

const user = {
  name: 'Fabio',
  surname: 'Biondi',
  address: {
    coords: {
      lat: 12,
      lng: 45
    }
  }
}

function doSomething({ address: { coords: { lat, lng }} }) {
  console.log(lat);
  console.log(lng);
}
// doSomething(user)


const list = ['x', 20]

function foo([a, b]) {
  console.log(a, b)
}




const formData = {
  name: 'Fabio',
  surname: 'Biondi',
  coords: { lat: 43, lng: 12 }
};

const { name: title, coords: { lat: lt, lng: lg, zoom: z = 5 }} = formData
const params = { title, lt, z}

console.log(params)




