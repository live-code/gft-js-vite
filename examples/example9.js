import axios from 'axios';

async function doSomething() {
  try {
    const response = await axios.get('https://jsonplaceholder.typicode.com/posts')
    console.log(response.data)
  } catch(e) {
    console.log('errore!!!! ', e.response)
  }

}

doSomething();


await axios.get('https://jsonplaceholder.typicode.com/posts')
  .then(res => console.log(res.data))
  .catch(err => console.log(err.response))


await axios.post('https://jsonplaceholder.typicode.com/posts/', {
  name: 'pippo'
})
  .then(res => console.log(res.data))
  .catch(err => console.log(err.response))
