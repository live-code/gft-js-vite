// ARRAY METHODS

const users = [
  {id: 11, name: 'Silvia', age: 16, gender: 'F', city: 'Gorizia'},
  {id: 22, name: 'Fabio', age: 17, gender: 'M', city: 'Trieste', zoom: 10},
  {id: 33, name: 'Lorenzo', age: 26, gender: 'M', city: 'Pordenone'},
  {id: 44, name: 'Lisa', age: 23, gender: 'F', city: 'Gorizia'}
];

// const ids = users
/*const adults = users.filter(user => user.age >= 18)
                    .map( ({id, ...rest}) => rest)
console.log(adults)*/

const URLPARAM = 33;
// const index = users.findIndex(user => user.id === URLPARAM)
// users.splice(index, 1)
// const newUsers = users.filter(user => user.id !== URLPARAM)

const total = users.reduce((acc, user) => acc + user.age, 0)

console.log(total)
