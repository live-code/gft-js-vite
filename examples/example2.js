import _ from 'lodash';

const user = {
  id: 1,
  name: 'Fabio',
  city: 'Gorizia',
  coords: {
    lat: 123,
    lng: 456,
    zoom: 1,
  },
}

// SHALLOW
const newState = clone(user)
newState.id = 123;
newState.coords.lat = 2423432

console.log( newState );


function clone(obj) {
  return JSON.parse(JSON.stringify(obj))
}
