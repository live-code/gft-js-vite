// arrow syntax

function add2(a, b) {
  return a + b;
}

const add3 = function(a, b) {
  return a + b;
}
// arrow (fat) syntax
const add = (a, b) => {
  return a + b;
}

const divide = (a, b) => a / b;
const double = a => a * a
const doNothing = () => console.log('do nothing');
setTimeout(() => doNothing(), 2000)

const getConfig2 = () => {
  return {
    title: 'bla bla',
    lang: 'it'
  }
}

const getConfig = () => ({ id: 123, title: 'bla bla', lang: 'it', city: '...' });

const { id, ...data } = getConfig();
console.log(data)



